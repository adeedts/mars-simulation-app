package id.co.adilahsoft.mars

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class MyBroadcastReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        if (context is MainActivity) {
            context.processMessage(intent)
        }
    }
}
