package id.co.adilahsoft.mars

import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.textfield.TextInputEditText
import java.net.URLEncoder
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import timber.log.Timber


class MainActivity : AppCompatActivity() {
    private val receiver = MyBroadcastReceiver()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btDownload = findViewById<Button>(R.id.btDownload)
        btDownload.setOnClickListener {
            action("download", 1)
        }

        val btUpload = findViewById<Button>(R.id.btUpload)
        btUpload.setOnClickListener {
            action("upload", 2)
        }

        val btCheckIn = findViewById<Button>(R.id.btCheckIn)
        btCheckIn.setOnClickListener {
            actionSurvey("checkin", 3)
        }

        val btSurvey = findViewById<Button>(R.id.btSurvey)
        btSurvey.setOnClickListener {
            actionSurvey("survey", 4)
        }

//        clickListener(R.id.llDownload, "download")
//        clickListener(R.id.llCheckin, "checkin")
//        clickListener(R.id.llUpload, "upload")

        val intentFilter = IntentFilter("indofood.survey.android.message")
        registerReceiver(receiver, intentFilter)
    }

    fun processMessage(intent: Intent) {
        val action = intent.getStringExtra("action")
        if ("upload" == action) {
            val message = String.format("sales: %s, customer: %s, uploaded: %s",
                intent.getStringExtra("salesCode"),
                intent.getStringExtra("customerCode"),
                intent.getBooleanExtra("uploaded", false).toString())

            Toast.makeText(this, message, Toast.LENGTH_LONG).show()
        }
    }

    private fun action(type: String, requestCode: Int) {
        val etSalesCode = findViewById<TextInputEditText>(R.id.etSalesCode)
        val etStockPointCode = findViewById<TextInputEditText>(R.id.etStockPointCode)

        val salesCode = etSalesCode.text.toString()
        val stockPointCode = etStockPointCode.text.toString()

        if (salesCode.isNotEmpty() && stockPointCode.isNotEmpty()) {
            sendIntent(
                URLEncoder.encode(salesCode, "UTF-8"),
                URLEncoder.encode(stockPointCode, "UTF-8"), type, requestCode
            )
        }
    }

    override fun onDestroy() {
        unregisterReceiver(receiver)

        super.onDestroy()
    }

    private fun actionSurvey(type: String, requestCode: Int) {
        val etSalesCodeCheckIn = findViewById<TextInputEditText>(R.id.etSalesCodeCheckIn)
        val etCustomerCode = findViewById<TextInputEditText>(R.id.etCustomerCode)

        val salesCodeCheckIn = etSalesCodeCheckIn.text.toString()
        val customerCode = etCustomerCode.text.toString()

        if (salesCodeCheckIn.isNotEmpty() && customerCode.isNotEmpty()) {
            val salesCodeCheckInParam = URLEncoder.encode(salesCodeCheckIn, "UTF-8")
            val customerCodeParam = URLEncoder.encode(customerCode, "UTF-8")

            val url = "indofoodsurvey://$type?salesCode=$salesCodeCheckInParam&customerCode=$customerCodeParam"
            val intent = Intent(Intent.ACTION_VIEW)

            intent.setPackage("indofood.survey.android")
            intent.data = Uri.parse(url)
            startActivityForResult(intent, requestCode)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == RESULT_OK) {
            if(requestCode == 1) {
                val downloaded = data?.getIntExtra("downloaded", requestCode)
                val downloadStatus = data?.getStringExtra("status_download")
                Toast.makeText(this, "berhasil mengambil $downloaded survey, Status: $downloadStatus", Toast.LENGTH_LONG).show()
            }
            if (requestCode == 2) {
                val json = data?.getStringExtra("data")
                val list = Gson().fromJson<List<BulkUploadResult>>(json, object : TypeToken<List<BulkUploadResult>>() {}.type)
                Toast.makeText(this, Gson().toJson(list), Toast.LENGTH_LONG).show()

            }
            if(requestCode == 3) {
                val checkInResult = data?.getIntExtra("status_survey", requestCode)
                val mandatory = data?.getStringExtra("mandatory")
                Toast.makeText(this, "status_survey: $checkInResult, mandatory: $mandatory", Toast.LENGTH_LONG).show()
            }
            if (requestCode == 4) {
                val surveyData = data?.getStringExtra("list_data")
                val list = Gson().fromJson<List<BulkUploadResult>>(surveyData, object : TypeToken<List<BulkUploadResult>>() {}.type)
                Toast.makeText(this, Gson().toJson(list), Toast.LENGTH_LONG).show()

            }
        }

        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun sendIntent(salescode: String, stockpointcode: String, to: String, requestCode: Int) {
//
//        val launchIntent = applicationContext.packageManager.getLaunchIntentForPackage("indofood.survey.android")
//        if (launchIntent != null) {
//            val sendIntent = Intent()
//            sendIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//            sendIntent.action = Intent.ACTION_SEND
//            val extras = Bundle()
//            //extras.putString("token", "isi dengan token")
//            extras.putString("salescode", salescode)
//            extras.putString("stockpointcode", stockpointcode)
//            extras.putString("to", to)
//            sendIntent.putExtras(extras)
//            sendIntent.type = "text/plain"
//            startActivity(sendIntent)
//        } else {
//            Toast.makeText(
//                applicationContext,
//                " launch Intent not available",
//                Toast.LENGTH_SHORT
//            ).show()
//        }
//
//        val sendIntent: Intent = Intent().apply {
//            action = Intent.ACTION_SEND
//            putExtra("salescode", salescode)
//            putExtra("stockpointcode", stockpointcode)
//            putExtra("to", to)
//            type = "text/plain"
//        }
//        val shareIntent = Intent.createChooser(sendIntent, null)
//        startActivity(shareIntent)

        val url = "indofoodsurvey://$to?salesCode=$salescode&stockpointCode=$stockpointcode"

        val intent = Intent(Intent.ACTION_VIEW)
        intent.setPackage("indofood.survey.android")
        intent.data = Uri.parse(url)
        startActivityForResult(intent, requestCode)

    }
}