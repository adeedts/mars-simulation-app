package id.co.adilahsoft.mars

data class BulkUploadResult(
    val customerCode: String,
    val status_survey: Int,
    val mandatory: String
)
